# Copyright (c) 2017 Remi Rampin
# Adapted from (original copyright below):
#     https://sqizit.bartletts.id.au/2011/02/14/pseudo-terminals-in-python/
# Copyright (c) 2011 Joshua D. Bartlett
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import array
import bisect
import fcntl
import os
import pty
import select
import signal
import sys
import termios
import time
import tty
import yaml


__version__ = '0.1'


def findlast(s, substrs):
    """ Finds whichever of the given substrings occurs last in the given string
    and returns that substring, or returns None if no such strings occur.
    """
    i = -1
    result = None
    for substr in substrs:
        pos = s.rfind(substr)
        if pos > i:
            i = pos
            result = substr
    return result


class LiveProcess(object):
    """ This class does the actual work of the pseudo terminal. The spawn()
    function is the main entrypoint.
    """

    def __init__(self, session):
        self.master_fd = None
        self.mode = 0
        self.buf = None
        self.queue = []
        self.hidden_chars = []
        self.session = session
        self.next_shortcut = 0

    def spawn(self, argv=None):
        """ Create a spawned process.

        Based on the code for pty.spawn().
        """
        assert self.master_fd is None
        if not argv:
            argv = [os.environ['SHELL']]

        pid, master_fd = pty.fork()
        self.master_fd = master_fd
        if pid == pty.CHILD:
            os.execlp(argv[0], *argv)

        old_handler = signal.signal(signal.SIGWINCH, self._set_pty_size)
        try:
            mode = tty.tcgetattr(pty.STDIN_FILENO)
            tty.setraw(pty.STDIN_FILENO)
        except tty.error:  # This is the same as termios.error
            sys.stderr.write("Couldn't use RAW terminal mode")
            sys.exit(1)
        self._set_pty_size()
        try:
            self._copy()
        finally:
            tty.tcsetattr(pty.STDIN_FILENO, tty.TCSAFLUSH, mode)

        os.close(master_fd)
        self.master_fd = None
        signal.signal(signal.SIGWINCH, old_handler)

    def _set_pty_size(self):
        """ Sets the window size of the child pty based on the window size of
        our own controlling terminal.
        """
        assert self.master_fd is not None

        # Get the terminal size of the real terminal, set it on the
        # pseudoterminal.
        buf = array.array('h', [0, 0, 0, 0])
        fcntl.ioctl(pty.STDOUT_FILENO, termios.TIOCGWINSZ, buf, True)
        fcntl.ioctl(self.master_fd, termios.TIOCSWINSZ, buf)

    def _copy(self):
        """ Main select loop.

        Passes all data to self.master_read() or self.stdin_read().
        """
        assert self.master_fd is not None
        master_fd = self.master_fd
        while True:
            now = time.time()
            try:
                if self.queue:
                    timeout = self.queue[0][0]
                    if timeout > now:
                        timeout -= now
                    else:
                        timeout = 0
                else:
                    timeout = None
                rfds, wfds, xfds = select.select([master_fd, pty.STDIN_FILENO],
                                                 [], [],
                                                 timeout)
            except select.error, e:
                if e[0] == 4:  # Interrupted system call.
                    continue

            if master_fd in rfds:
                data = os.read(self.master_fd, 1024)
                if not data:
                    break  # Stream closed, exit
                self.master_read(data)
            if pty.STDIN_FILENO in rfds:
                data = os.read(pty.STDIN_FILENO, 1024)
                self.stdin_read(data)
            if self.queue and self.queue[0][0] <= now:
                _, f, kwargs = self.queue.pop(0)
                f(**kwargs)

    def slow(self, func, data, **kwargs):
        t = time.time()
        for c in data:
            t = t + 0.1
            entry = t, func, {'data': c}
            idx = bisect.bisect(self.queue, entry)
            self.queue.insert(idx, entry)

    def write_stdout(self, data):
        """ Writes to stdout as if the child process had written the data.
        """
        os.write(pty.STDOUT_FILENO, data)

    def write_master(self, data):
        """ Writes to the child process from its controlling terminal.
        """
        master_fd = self.master_fd
        assert master_fd is not None
        while data != '':
            n = os.write(master_fd, data)
            data = data[n:]

    def master_read(self, data):
        """ Called when there is data to be sent from the child process back to
        the user.
        """
        if self.hidden_chars:
            i = 0
            while self.hidden_chars and i < len(data):
                if data[i:i+1] == self.hidden_chars.pop(0):
                    i += 1
                else:
                    self.hidden_chars = []
                    break
            data = data[i:]
        if data:
            self.write_stdout(data)

    def stdin_read(self, data):
        """ Called when there is data to be sent from the user/controlling
        terminal down to the child process.
        """
        while data:
            if self.mode == 0:
                pos = data.find(b'\x14')  # Ctrl-T
                if pos == -1:
                    self.write_master(data)
                    break
                else:
                    self.write_master(data[:pos])
                    data = data[pos + 1:]
                    self.mode = 1
            elif self.mode == 1:
                if data[0] == b't':
                    self.shortcut()
                    data = data[1:]
                    self.mode = 0
                elif data[0] == b'd':
                    data = data[1:]
                    self.buf = []
                    self.mode = 2
                else:
                    self.write_master(b'\x14')
                    self.mode = 0
            elif self.mode == 2:
                if data[0] == b'\x0d':
                    name = ''.join(self.buf)
                    self.buf = None
                    data = data[1:]
                    self.mode = 0
                    try:
                        self.next_shortcut = self.session.shortcut_index[name]
                    except KeyError:
                        self.write_master(b'?')
                    else:
                        self.shortcut()
                elif data[0].isalnum():
                    self.buf.append(data[0])
                    data = data[1:]
                else:
                    self.write_master(b'n')
                    self.buf = None
                    self.mode = 0

    def shortcut(self):
        if self.next_shortcut >= len(self.session.shortcuts):
            self.write_master(b'!')
            return

        steps = self.session.shortcuts[self.next_shortcut]
        if steps:
            func, kwargs = steps[0]
            func(self, **kwargs)

        self.next_shortcut += 1


class InvalidSession(ValueError):
    pass


class Session(object):
    def __init__(self, filename):
        try:
            with open(filename, 'r') as fp:
                raw_obj = yaml.load(fp)
        except IOError:
            raise InvalidSession("Couldn't open session file")

        if 'shortcuts' not in raw_obj:
            raise InvalidSession("No key 'shortcuts' in session")
        if not isinstance(raw_obj['shortcuts'], list):
            raise InvalidSession("'shortcuts' should be an array")

        self.shortcuts = []
        self.shortcut_index = {}

        for shortcut in raw_obj['shortcuts']:
            if not isinstance(shortcut, dict) or len(shortcut) != 1:
                raise InvalidSession("'shortcuts' entries should be "
                                     "{name: ...}")
            name = next(iter(shortcut))
            if name in self.shortcut_index:
                raise InvalidSession("Multiple shortcuts '%s'" % name)
            steps = shortcut[name]
            if isinstance(steps, dict):
                steps = [steps]
            elif not isinstance(steps, list):
                raise InvalidSession("shortcut '%s' should contain a step or "
                                     "list of steps" % name)
            parsed_steps = []
            for i, step in enumerate(steps):
                if i > 0:
                    raise InvalidSession("Only one step per shortcut is "
                                         "supported currently. WIP!")

                if not isinstance(step, dict):
                    raise InvalidSession("Step should be an object: "
                                         "'%s' #%d" % (name, i + 1))

                cmd = None
                for key in step.keys():
                    if hasattr(self, 'cmd_' + key):
                        if cmd is not None:
                            raise InvalidSession("Invalid step: '%s' #%d" % (
                                                 name, i + 1))
                        cmd = getattr(self, 'cmd_' + key)
                if cmd is None:
                    raise InvalidSession("Step has no command: '%s' #%d" % (
                                         name, i + 1))
                parsed_steps.append((cmd, step))

            self.shortcut_index[name] = len(self.shortcuts)
            self.shortcuts.append(parsed_steps)

    @staticmethod
    def cmd_input(process, input, slow=None, hidden=False):
        if slow is None:
            slow = not hidden
        if slow and hidden:
            raise ValueError("slow=true hidden=true is not supported")
        if hidden:
            process.hidden_chars = [input[i:i+1] for i in range(len(input))]
        if slow:
            process.slow(process.write_master, input)
        else:
            process.write_master(input)

    @staticmethod
    def cmd_output(process, output, slow=False):
        if slow:
            process.slow(process.write_stdout, output)
        else:
            process.write_stdout(output)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.stderr.write("Usage: liecoding.py <session.yaml> [program ...]\n")
        sys.exit(1)
    try:
        session = Session(sys.argv[1])
    except InvalidSession as e:
        sys.stderr.write("Invalid session: %s\n" % e.message)
        sys.exit(1)
    proc = LiveProcess(session)
    proc.write_stdout("\nliecoding.py started.\n")
    proc.spawn(sys.argv[2:])
    proc.write_stdout("\nliecoding.py exiting.\n")
