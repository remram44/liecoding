LieCoding
=========

LieCoding is a helper tool for live coding demos. Rather than having to type commands from memory, or showing a video, LieCoding is an intermediate option that will use the original program but type the input for you when you press a button.

You simply have to install LieCoding and provide it with a simple file listing the commands you'll have to enter. Press a button, and LieCoding enters the next input.

Example session file
--------------------

Session files are YAML files listing the inputs. A few other actions can be performed, like printing fake output, customizing the typing speed, and more to come.

..  code-block:: yaml

    # A session contains multiple shortcuts that are triggered through Ctrl-T
    shortcuts:
      # Shortcuts are ordered, starting at the first input and advancing through
      # the list each time 'Ctrl-T t' is used
      # Each input also has a name, which is used to quickly jump somewhere in the
      # list using 'Ctrl-T d <name><enter>'

      # A shortcut only needs the input to type for you
      # By default, slow=true which means that the input will be entered slowly, as
      # if typed on the keyboard
      - hello:
        - input: "echo \"Hello, world\""

      # Another command. Note that for it to be run directly, a \n has to be added
      - exit:
        - input: "exit\n"
