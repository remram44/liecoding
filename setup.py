import io
import os
from setuptools import setup


# pip workaround
os.chdir(os.path.abspath(os.path.dirname(__file__)))


# Need to specify encoding for PY3, which has the worst unicode handling ever
with io.open('README.rst', encoding='utf-8') as fp:
    description = fp.read()
req = [
    'PyYAML']
setup(name='liecoding',
      version='0.1',
      py_modules=['liecoding'],
      entry_points={
          'console_scripts': [
              'liecoding = liecoding:main']},
      install_requires=req,
      description="Helper for live coding demos: terminal wrapper that will type commands for you, as you press a shortcut",
      author="Remi Rampin",
      author_email='remirampin@gmail.com',
      maintainer="Remi Rampin",
      maintainer_email='remirampin@gmail.com',
      url='https://gitlab.com/remram44/liecoding',
      long_description=description,
      license='BSD-3-Clause',
      keywords=['live', 'demo', 'live demo', 'coding', 'live coding'],
      classifiers=[
          'Development Status :: 1 - Planning',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'Intended Audience :: Education',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: BSD License',
          'Operating System :: POSIX',
          'Programming Language :: Python :: 2.7',
          'Topic :: Communications',
          'Topic :: Education',
          'Topic :: Multimedia :: Graphics :: Presentation',
          'Topic :: System :: Shells',
          'Topic :: Terminals',
          'Topic :: Utilities'])
